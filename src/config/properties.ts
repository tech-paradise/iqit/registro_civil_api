// import * as process from "process";

import {Config} from "knex";
import {IGeneratorSchema, ISchemaDb} from "./ISchemaDb";

export  class Properties {

    // Configurando la variable global process.env
    public static init(): void {
        let schemaDB: Config;
        let schemaDBCustom: Config;

        process.env.EMAIL = "tech.paradise.cancun@gmail.com";
        process.env.EMAIL_PASS = "cachemora";

        // PORT server
        process.env.PORT = process.env.PORT  || "3000";
        //  URL server
        process.env.URL = process.env.URL || "http://187.157.146.150:3000";
        process.env.URL_DB = process.env.URL_DB || "187.157.146.150";
        process.env.URL_DB_CUSTOM = process.env.URL_DB_CUSTOM || "34.73.116.77";
        //  Entorno
        process.env.NODE_ENV = process.env.NODE_ENV || "devPg";
        //  Vencimiento del Token
        // 60 segundos
        // 60 minutos
        // 24 horas
        // 30 días
        // process.env["CADUCIDAD_TOKEN"] = "168h";
        process.env.CADUCIDAD_TOKEN_EMAIL = "7d";
        //  SEED de autenticación

        process.env.CRYPT_SALT = process.env.CRYPT_SALT || "10";
        process.env.JWT_SEDD = process.env.JWT_SEDD || "sed-de-api-registro-civil";

        // ============================
        //  Base de datos
        //  Direccion de servidor
        // ============================
        schemaDB = {
            client: "mssql",
            connection: {
                database: "interconexion",
                host: process.env.URL_DB,
                password: "qr0023",
                user: "QR0023"
            },
            debug: true,
            acquireConnectionTimeout: 300000
        };

        schemaDBCustom = {
            client: "pg",
            connection: {
                database: "registro_api_dev",
                host: process.env.URL_DB_CUSTOM,
                password: "cachemora",
                user: "registro"
            },
            debug: true
        };
        process.env.SCHEMA_GENERATOR_SQL = JSON.stringify(schemaDB);
        process.env.SCHEMA_GENERATOR_SQL_CUSTOM = JSON.stringify(schemaDBCustom);
    }
}
