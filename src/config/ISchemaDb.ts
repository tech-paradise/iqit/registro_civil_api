export interface ISchemaDb {
    user: string;
    host: string|undefined;
    database: string;
    password: string;
    port: number;
}

export interface IGeneratorSchema {
    client: string;
    connection: {
        host: string;
        user: string;
        password: string;
        database: string;
    };
}
