import cookieParser from "cookie-parser";
import express, {Application, NextFunction, Request, Response, Router} from "express";
import hbs from "hbs";
import createError, {HttpError} from "http-errors";
import path from "path";
import ApiRoutes from "../routes/ApiV1";
import * as Rutas from "../routes/RoutesNames";

export default class Server {

    public static init(port: number): Server {
        return new Server(port);
    }
    public app: Application;
    private readonly mRouter: Router;

    constructor(private port: number) {
        this.app = express();
        this.mRouter = new ApiRoutes().mRouter;
    }

    public start( callback?: (...args: any[]) => void) {
        this.config();
        // poner a escuchar al sever
        this.app.listen(this.port, callback);
    }

    private config(): void {
        // establece el directorio de las vistas
        this.app.set("views", path.join(process.cwd(), "views"));
        this.app.set("view engine", "hbs");
        hbs.registerHelper("switch", function(value, options) {
            this.switch_value = value;
            return options.fn(this);
        });

        hbs.registerHelper("case", function(value, options) {
            if (value === this.switch_value) {
                return options.fn(this);
            }
        });

        // establece utilidades para el req.body
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());

        // establece el directorio estatico
        this.app.use(express.static(path.join(process.cwd(), "public")));

        // index de las rutas
        this.app.use(Rutas.API_V1, this.mRouter);

        // Last middleware, in case not match whit a route, throws a http-error
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            next(createError(404));
        });

        // Error handler, redirect a page with the error
        this.app.use((err: HttpError, req: Request, res: Response, next: NextFunction) => {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = err;

            // render the error page
            res.status(err.status || 500);
            res.render("error");
        });
    }
}
