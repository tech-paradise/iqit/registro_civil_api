import fs from "fs";
import hbs from "hbs";
import mailer from "nodemailer";
import path from "path";

export  interface IMail {
    from: string;
    to: string;
    subject: string;
}

export interface IMailConfirmation {
    context: any;
    template: string;
}

interface IMailOptions extends IMail {
    html: string;
}

export const sendMail = async (mail: IMail, context: IMailConfirmation): Promise<boolean> => {
    const transporter = mailer.createTransport({
            service: "gmail",
            auth: {
                user: process.env.EMAIL,
                pass: process.env.EMAIL_PASS
            }
        }
    );
    const template: string = fs.readFileSync(path.join(process.cwd(), context.template), "utf8");
    const emailTemplate: HandlebarsTemplateDelegate<any> = hbs.handlebars.compile(template);
    const mailOptions: IMailOptions = {
        from: mail.from,
        to: mail.to,
        subject: mail.subject,
        html: emailTemplate(context.context)
    };
    try {
        await transporter.sendMail(mailOptions);
    } catch (e) {
        return false;
    }
    return true;
};
