import {Result} from "express-validator";
import {validationResult} from "express-validator";
import {Request} from "express-validator/src/base";
import moment from "moment";

export function getNow(): string {
    return `[${moment().format("YYYY/MM/DD-HH:mm:ss")}] api_registro_civil : `;
}

export const setHeaderEndPoint = (endpoint: string, body: string): void => {
    console.log(`${getNow()}+++++++++++++++++++++++ En endpoint ${endpoint}`);
    console.log(getNow(), "Body recibido:\n", body);
};

export const setErrorParam = (result: Result ): void => {
    const params: string = result.array().map((i) => `${JSON.stringify(i.param)}`).join("\n");
    console.log(`${getNow()}-------- Parametros erroneos: \n${params}`);
};

/*const getHeaderParam = (): void =>{
    console.log(`${getNow()}`);
};*/

export const setError = (error: string): void => {console.log(`${getNow()}-------- Error ${error}`); };

export const setMessage = ( messg: string): void => {console.log(`${getNow()}${messg}`); };

export const setSucess = (success: string): void => {console.log(`${getNow()}++++++++ Hecho ${success}`); };

export const validateParams = async (req: Request): Promise<string> => {
    const result = await validationResult (req);
    setMessage("Validando parámetros");
    if (!result.isEmpty()) {
        const error = result.array().map((i) => `error en ${i.param} ${i.msg}`).join(", ");
        setErrorParam(result);
        return error;
    } else {
        return null;
    }
};
