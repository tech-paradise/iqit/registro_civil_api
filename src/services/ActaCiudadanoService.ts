import Knex, {QueryBuilder} from "knex";
import * as timeStamp from "../helpers/LogUtils";
import {IActaCiudadano} from "../models/IActaCiudadanoModel";

export default class ActaCiudadanoService {
    private readonly knex: Knex = Knex(JSON.parse(process.env.SCHEMA_GENERATOR_SQL));
    private readonly tablaNacimientos: QueryBuilder = this.knex("dbo.nrc_nacimientos");

    public async findActaInfo(cadena: string): Promise<IActaCiudadano> {
        const registro: Knex.QueryBuilder<any, any> = this.tablaNacimientos.select("nrc_nacimientos.*",
            this.knex.ref("NRC_ESTADO.ENT_DESCRIPCION").as("edo_registro"),
            this.knex.ref("NRC_MUNICIPIO.MUN_DESCRIPCION").as("mun_registro"))
            .join("NRC_ESTADO", "nrc_nacimientos.ENTIDADREGISTRO", "NRC_ESTADO.ENT_CODIGO")
            .join("NRC_MUNICIPIO", function() {
                this.on( "nrc_nacimientos.MUNICIPIOREGISTRO", "NRC_MUNICIPIO.MUN_CODIGO")
                    .andOn("NRC_MUNICIPIO.ENT_CODIGO", "NRC_ESTADO.ENT_CODIGO");
            })
            .where("nrc_nacimientos.CADENA", cadena)
            .as("a");

        const nacimiento: Knex.QueryBuilder<any, any> = this.knex.select("a.*",
                this.knex.ref("NRC_ESTADO.ENT_DESCRIPCION").as("nacimiento"))
            .from("NRC_ESTADO")
            .rightJoin(registro,
                "NRC_ESTADO.ENT_CODIGO",
                "a.pe_entidadnacimiento")
            .as("b");

        const paisPa: Knex.QueryBuilder<any, any> = this.knex.select("b.*",
                    this.knex.ref("NRC_PAIS.PAI_NACIONALIDAD").as("pa_nacimiento"))
            .from("NRC_PAIS")
            .join(nacimiento,
                "NRC_PAIS.PAI_CODIGO",
                "b.pa_nacionalidad")
            .as("c");

        const paisMa: Knex.QueryBuilder<IActaCiudadano[], any> = this.knex.select("c.*",
            this.knex.ref("NRC_PAIS.PAI_NACIONALIDAD").as("ma_nacimiento"))
            .from("NRC_PAIS")
            .join(paisPa,
                "NRC_PAIS.PAI_CODIGO",
                "c.ma_nacionalidad");
        let actas: IActaCiudadano[];
        try {
            actas = await paisMa.then();
        } catch (e) {
            timeStamp.setError(`En obtener acta: ${e}`);
            return null;
        }
        return  actas.length === 1 ? actas[0] : null;
    }

    public async actaExist(cadenaActa: string = ""): Promise<boolean> {
        const query: QueryBuilder<unknown, any> = this.knex.select("CADENA")
            .from("dbo.nrc_nacimientos")
            .where("CADENA", cadenaActa);
        let acta: string[];
        try {
            acta = await query.then();
        } catch (e) {
            return false;
        }
        return acta.length === 1;
}
}
