import Knex, {QueryBuilder} from "knex";
import * as timeStamp from "../helpers/LogUtils";
import {ICiudadanoDBModel} from "../models/ICiudadanoModels";

// TODO que pasa cuando existe duplicidad de informacion curp
export default class CiudadanoService {
    private readonly knex: Knex = Knex(JSON.parse(process.env.SCHEMA_GENERATOR_SQL));
    private readonly tablaNacimientos: QueryBuilder = this.knex("dbo.nrc_nacimientos");

    public async findByCurpInfo(
          curp: string = ""
        , nombresPadre: string = ""
        , primerApellidoPadre: string = ""
        , segundoApellidoPadres: string = ""): Promise<ICiudadanoDBModel> {

        const query: QueryBuilder<ICiudadanoDBModel> =
            this.tablaNacimientos
            .where("pe_curp", curp)
            .andWhere(function() {
                this.where(function() {
                    this.where("pa_nombres", nombresPadre)
                        .andWhere("pa_primerapellido", primerApellidoPadre)
                        .andWhere("pa_segundoapellido", segundoApellidoPadres);
                })
                    .orWhere(function() {
                        this.where("ma_nombres", nombresPadre)
                            .andWhere("ma_primerapellido", primerApellidoPadre)
                            .andWhere("ma_segundoapellido", segundoApellidoPadres);
                    });
            });
        let ciudadano: ICiudadanoDBModel[];
        try {
            ciudadano = await query.then((row) => row);
        } catch (e) {
            timeStamp.setError(`Error al consultar: ${e}`);
        }
        return ciudadano.length > 0 ? ciudadano[0] : null;

    }

    public async findByInfo(
          nombre: string = ""
        , primerApellido: string = ""
        , segundoApellido: string = ""
        , sexo: string = ""
        , numeroEntidad: number = 0
        , fechaNac: string = "0000-00-00"
        , nombresPadre: string = ""
        , primerApellidoPadre: string = ""
        , segundoApellidoPadres: string = ""): Promise<ICiudadanoDBModel> {

        const query: QueryBuilder<ICiudadanoDBModel> =
            this.tablaNacimientos
                .where({
                    pe_nombres: nombre,
                    pe_primerapellido: primerApellido,
                    pe_segundoapellido: segundoApellido
                })
                .andWhere(function() {
                    this.where(function() {
                        this.where("pa_nombres", nombresPadre)
                            .andWhere("pa_primerapellido", primerApellidoPadre)
                            .andWhere("pa_segundoapellido", segundoApellidoPadres);
                    })
                        .orWhere(function() {
                            this.where("ma_nombres", nombresPadre)
                                .andWhere("ma_primerapellido", primerApellidoPadre)
                                .andWhere("ma_segundoapellido", segundoApellidoPadres);
                        });
                });
        let ciudadano: ICiudadanoDBModel[];
        try {
            ciudadano = await query.then((row) => row);
            console.log("here----->", ciudadano);
        } catch (e) {
            timeStamp.setError(`Error al consultar: ${JSON.stringify(e)}`);
        }
        return ciudadano.length > 0 ? ciudadano[0] : null;

    }

    public async ciudadanoByCurp(curp): Promise<ICiudadanoDBModel[]> {
        const query: QueryBuilder<ICiudadanoDBModel> =
            this.tablaNacimientos
                .where("pe_curp", curp);
        let ciudadanos: ICiudadanoDBModel[];
        try {
            ciudadanos = await query.then((row) => row);
        } catch (e) {
            timeStamp.setError(`Error al consultar: ${e}`);
        }
        return ciudadanos.length > 0
            ? ciudadanos
            : null;
    }
}
