import {Request} from "express";
import Knex, {QueryBuilder} from "knex";
import * as timeStamp from "../helpers/LogUtils";
import {ICiudadanoDBModel} from "../models/ICiudadanoModels";
import {ICiudadanoCustomDB, ILoginCustomDB, IUserCustomDB} from "../models/IUserModel";

export default class UserService {
    private readonly KNEX_CUSTOM: Knex = Knex(JSON.parse(process.env.SCHEMA_GENERATOR_SQL_CUSTOM));
    private readonly LOGIN_TBL: QueryBuilder = this.KNEX_CUSTOM("interconexion.login");
    private readonly CIUDADANO_TBL: QueryBuilder = this.KNEX_CUSTOM("interconexion.ciudadano");
    private readonly CATALOG_CIUDADANO: QueryBuilder = this.KNEX_CUSTOM("interconexion.catalogo_ciudadanos");
    private readonly QRY_USER: QueryBuilder = this.KNEX_CUSTOM("interconexion.qry_users");

    public async emailExist(email: string): Promise<boolean> {
        console.log("entre email----->");
        const query: QueryBuilder<ICiudadanoDBModel> =
            this.LOGIN_TBL
                .where("correo", email);
        let user: IUserCustomDB[];
        try {
            user = await query.then();
            console.log("email----->", user);
        } catch (e) {
            timeStamp.setError(`En email exist: ${e}`);
        }
        return user.length > 0;
    }

    public async createUser(req: Request): Promise<IUserCustomDB> {
        const {name, fName, sName, curp= null, cellphone, idDevice, email, genero, password} = req.body;
        const transaction = this.KNEX_CUSTOM.transaction(async (trx) => {
            try {
                const ciudano: ICiudadanoCustomDB[] =
                    await this.CIUDADANO_TBL.transacting(trx).insert({nombre: name, primer_apellido: fName,
                        segundo_apellido: sName, curp , genero}).returning("*").then();
                console.log("ciu-------->", ciudano);
                const login: ILoginCustomDB[] = await  this.LOGIN_TBL.transacting(trx)
                    .insert({device_id: idDevice, correo: email, password,
                    telcel: cellphone}).returning("*").then();
                console.log("log-------->", login);
                const catalog = await  this.CATALOG_CIUDADANO.transacting(trx)
                    .insert({login_id_login: login[0].id_login,
                    ciudadano_id_ciudadano: ciudano[0].id_ciudadano, propietario: true}).returning("*").then();
                const  res: IUserCustomDB = {id: login[0].id_login,
                    correo: login[0].correo,
                    nombre: ciudano[0].nombre,
                    apellido: ciudano[0].primer_apellido};
                await trx.commit(res);
                console.log("cat-------->", catalog);
            } catch (e) {
                timeStamp.setError("En transaccion crear usuario: " + e);
                await trx.rollback();
            }
        } );
        try {
            const aux: IUserCustomDB = await transaction.then();
            console.log("crete------>", aux);
            return aux;
        } catch (e) {
            timeStamp.setError("en transaccion crear usuario: " + e);
            return null;
        }
    }

    public async valideUserById(userId: number): Promise<IUserCustomDB> {
        const queryUpdate: QueryBuilder = this.LOGIN_TBL
            .where({id_login: userId})
            .update({verified: true});

        const queryViewUser: QueryBuilder<IUserCustomDB, object> = this.QRY_USER
            .first(this.KNEX_CUSTOM.ref("id_login").as("id")
            , "correo"
            , "nombre"
            , this.KNEX_CUSTOM.ref("primer_apellido").as("apellido"))
            .where("id_login", userId);
        let user: IUserCustomDB;
        try {
            await queryUpdate.then();
            user = await queryViewUser.then();
        }  catch (e) {
            timeStamp.setError(e);
            return  null;
        }
        return user;
    }

    /*public async  getLoginCurpOrEmail(info: {email?: string; curp?: string; }) {
        if (info.email) {
            await  this.QRY_USER.select()
        } else if (info.curp){

        }
        return null;
    }*/
}
