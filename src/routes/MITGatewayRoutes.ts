import {Request, Response, Router} from "express";
import {body, ValidationChain} from "express-validator";
import MITGatwayControlles from "../controllers/MITGatwatController";
import * as Rutas from "./RoutesNames";

export default class MITGatewayRoutes {
    private readonly routes: Router = Router();
    private readonly VALIDATE_MIT: string = "mit";

    constructor() {
        this.init();
    }

    public getRoutes(): Router {
        return this.routes;
    }

    private init(): void {
        const mitGatwatController: MITGatwayControlles = new MITGatwayControlles();
        this.routes.post(Rutas.MIT, this.validtaeSearch(this.VALIDATE_MIT),
            (req: Request, res: Response) => mitGatwatController.payment(req, res));
        this.routes.post(Rutas.MIT + Rutas.FINISH_PAYMENT,
            (req: Request, res: Response) => mitGatwatController.finish(req, res));
    }

    private validtaeSearch(method: string): ValidationChain[] | ValidationChain {
        switch (method) {
            case this.VALIDATE_MIT: {
            return body("amount", "--> el monto del pago es campo obligatorio ").isFloat();
            }
        }
    }
}
