import {Request, Response, Router} from "express";
import {check, ValidationChain} from "express-validator";
import CiudadanoController from "../controllers/CiudadanoController";
import * as Rutas from "./RoutesNames";

export enum TypeParameter {
    CURP = "CURP",
    DATA = "DATA",
}

export default class  CiudadanoRoutes {

    private readonly routes: Router = Router();

    constructor() {
        this.init();
    }
    public  getRoutes(): Router {
        return this.routes;
    }

    private validtaeSearch(): ValidationChain[] {
        return[
            check("type", "--> tipo de operacion no aceptada").trim().custom((val, {req}) => {
                if (req.body.type.toUpperCase() in TypeParameter) {
                    req.body.type = TypeParameter [req.body.type.toUpperCase()];
                } else {
                    throw new Error("--> tipo de operacion no aceptada");
                }
                return true;
            }),
            check("nombresPadre", "--> nombre campo obligatorio").isString().trim(),
            check("primerApellidoPadre", "--> primer apellido campo obligatorio").isString().trim(),
            check("segundoApellidoPadre", "--> segundo apellido campo obligatorio").isString().trim(),
            check("curp", "--> curp campo obligatorio")
                .if((val, {req}) => req.body.type === TypeParameter.CURP)
                .isString().trim(),
            check("sexo", "--> el campo sexo no tiene un dato valido")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .trim().isIn(["m", "M", "F", "f"]),
            check ("numeroEntidad", "--> el campo numero de entidad es obligatorio y entero")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .trim().isInt(),
            check("fechaNacimiento", "--> la fecha de nacimiento con formato incorrecto")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .trim().isISO8601(),
            check("nombres", "--> los nombres son un campo obligatorio")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .isString().trim(),
            check("primerApellido", "--> el primer apellido es un campo obligatorio")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .isString().trim(),
            check("segundoApellido", "--> el segundo apellido campo obligatorio")
                .if((val, {req}) => req.body.type === TypeParameter.DATA)
                .isString().trim()
        ];
    }

    // Cargando rutas
    private init(): void {
        const ciudadanoController: CiudadanoController = new CiudadanoController();
        // console.log(validate.validateCiudadano(validate.VALIDATE_CURP));
        this.routes.get(Rutas.ROOT, (req: Request, res: Response) => {
            res.json({message: "Ciudadano works!!!"});
        });

        this.routes.post(Rutas.SEARCH_CIUDADANO, this.validtaeSearch(),
            (req: Request, res: Response) => ciudadanoController.buscarCiudadano(req, res));
    }
}
