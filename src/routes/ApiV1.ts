import {Request, Response, Router} from "express";
import ActaCiudadanoRoutes from "./ActaCiudadanoRoutes";
import CiudadanoRoutes from "./CiudadanoRoutes";
import MITGatewayRoutes from "./MITGatewayRoutes";
import * as Rutas from "./RoutesNames";
import UserRoutes from "./UserRoutes";

// Rutas pruincipales para Api v1
export default class ApiRoutes {
    public readonly mRouter: Router = Router();

    constructor() {
        this.init();
    }
// Cargando rutas
    private init(): void {
        this.mRouter.get(Rutas.ROOT, (req: Request, res: Response) => {
            res.json({message: "Api works!!!"});
        });
        this.mRouter.use(Rutas.CIUDADANO, new CiudadanoRoutes().getRoutes());
        this.mRouter.use(Rutas.USER, new UserRoutes().getRoutes());
        this.mRouter.use(Rutas.CIUDADANO, new ActaCiudadanoRoutes().getRoutes());
        this.mRouter.use(Rutas.PAYMENT, new MITGatewayRoutes().getRoutes());
    }
}
