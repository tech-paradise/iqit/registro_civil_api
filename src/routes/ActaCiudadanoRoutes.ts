import {Request, Response, Router} from "express";
import {check, ValidationChain} from "express-validator";
import ActaCiudadanoController from "../controllers/ActaCiudadanoController";
import ActaCiudadanoService from "../services/ActaCiudadanoService";
import * as Rutas from "./RoutesNames";

export default class  ActaCiudadanoRoutes {
    private readonly routes: Router = Router();
    private readonly actaCiudadanoController: ActaCiudadanoController = new ActaCiudadanoController();
    private readonly actaCiudadanoService: ActaCiudadanoService = new ActaCiudadanoService();
    constructor() {
        this.init();
    }

    public getRoutes(): Router {
        return  this.routes;
    }

    private init(): void {
        this.routes.post(Rutas.CIUDADANO_ACTAS, this.validateCiudadaoActa()
            , async (req: Request, res: Response) => this.actaCiudadanoController.getActa(req, res));
    }

    private validateCiudadaoActa(): ValidationChain[] {
        return [check("cadena", "--> es campo obligatorio").isString().trim()
            .custom(async (cadena) => {
                console.log("---------->", cadena);
                if (!await this.actaCiudadanoService.actaExist(cadena)) {
                    throw new Error("No existe informacion de esta acta");
                }
                return true;
            })];
    }
}
