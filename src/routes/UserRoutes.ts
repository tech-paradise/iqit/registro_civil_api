import {Request, Response, Router} from "express";
import {body, ValidationChain} from "express-validator";
import UserController from "../controllers/UserController";
import {ICiudadanoDBModel} from "../models/ICiudadanoModels";
import CiudadanoService from "../services/CiudadanoService";
import UserService from "../services/UserService";
import * as Rutas from "./RoutesNames";

// This because password-validator doesnt have a type definitions
// tslint:disable-next-line:no-var-requires
const PassValidator: any = require ("password-validator");

export default class  UsersRoutes {
    private readonly routes: Router;
    private readonly schema;
    private readonly VALIDATE_CREATE_USER: string = "createUser";
    private readonly VALIDATE_LOGIN: string = "loginUser";

    constructor() {
        this.routes = Router();
        this.schema = new PassValidator()
            .is().min(8)
            .is().max(16)
            .has().digits();
        this.init();
    }

    public  getRoutes(): Router {
        return this.routes;
    }

    // Routes
    private init(): void {
        const userController: UserController = new UserController();
        this.routes.get(Rutas.ROOT, (req: Request, res: Response) => {
            res.json({message: "User works!!!"});
        });
        this.routes.post(Rutas.USER_REGISTER, this.validateUser(this.VALIDATE_CREATE_USER),
            (req: Request, res: Response) => userController.createUser(req, res));
        this.routes.get(Rutas.USER_VALIDATE,
            (req: Request, res: Response) => userController.validateEmail(req, res));
        this.routes.post(Rutas.USER_LOGIN,
            (req: Request, res: Response) => userController.login(req, res));
    }

    // Validations Routes for this class
    private validateUser(method: string): ValidationChain[] {
        const ciudadanoModel: CiudadanoService = new CiudadanoService();
        const userModel: UserService = new UserService();
        switch (method) {
            case this.VALIDATE_CREATE_USER: {
                return [body("name", "nombre invalido").isString().trim(),
                    body("fName", "apellido invalido")
                        .isString()
                        .trim(),
                    body("sName")
                        .isString()
                        .trim(),
                    body("curp", "es campo obligatorio")
                        .optional()
                        .isString()
                        .custom(async (curp: string, {req}): Promise<boolean> => {
                            let ciudadanos: ICiudadanoDBModel[] = await ciudadanoModel
                                .ciudadanoByCurp(curp.toUpperCase());
                            if (!ciudadanos) {
                                throw new Error("no existe Curp");
                            } else if (ciudadanos.length > 1) {
                                throw new Error("esta curp esta asignada a mas de una persona");
                            }
                            ciudadanos = ciudadanos.filter((ciu) =>
                                req.body.name.toUpperCase() === ciu.PE_NOMBRES
                                && req.body.fName.toUpperCase() === ciu.PE_PRIMERAPELLIDO
                                && req.body.sName.toUpperCase() === ciu.PE_SEGUNDOAPELLIDO
                            );
                            if (ciudadanos.length === 0) {
                                throw  new
                                Error("el nombre o los apellidos no coinciden con la informacion de la curp");
                            }
                            return true;
                        }),
                    body("cellphone", "es campo obligatorio")
                        .isMobilePhone("any"),
                    body("idDevice", "es campo obligatorio").exists(),
                    body("email", "email invalido")
                        .isEmail()
                        .if(( val, {req}) => req.body.email)
                        .custom(async (email: string): Promise<boolean> => {
                            const em = await userModel.emailExist(email);
                            if (em) {
                                throw new Error("email registrado");
                            }
                            return true;
                        }),
                    body("genero", "es campo obligatorio")
                        .isIn(["f", "m", "F", "M"]),
                    body("password")
                        .custom((password: string): boolean => {
                            if (!this.schema.validate(password)) {
                                throw new Error("password debe tener entre 8 y 16 caracteres, algún numero y constra");
                            }
                            return true;
                        }),
                    body("terminos", "debe ser booleano")
                        .isBoolean().toBoolean()
                        .custom((terminos: boolean): boolean => {
                            if (terminos === false) {
                                throw new Error("se debe aceptar los terminos");
                            }
                            return true;
                        })
                ];
            }
            case this.VALIDATE_LOGIN: {
                return [body("email", "formato invalido").isEmail(),
                    body("password", "obligatorio").exists()];
            }
        }
    }
}
