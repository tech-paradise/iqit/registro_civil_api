export const ROOT = "/";
export const LOCALHOST = "http://localhost:3000";

/* routes for api v1*/
export const API_V1 = "/api/v1";

/* routes for ciudadano */
export const CIUDADANO = "/ciudadano";
export const SEARCH_CIUDADANO = "/search"; // This because implement post instead of get

/* routes for user */
export const USER = "/user";
/* TODO for register user, only should use route USER,
    but the method post is taken in a read operation*/
export const USER_REGISTER = "/register";
export const USER_VALIDATE = "/register/:token";
export const USER_LOGIN = "/session";
// export const

/* routes for actas for ciudadano*/
export  const  CIUDADANO_ACTAS = "/acta";

/* routes for payment MIT */
export const PAYMENT = "/payment";
export const MIT = "/MIT";
export const FINISH_PAYMENT = "/finish";
