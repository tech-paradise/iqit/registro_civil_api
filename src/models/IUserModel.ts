export interface IUserCustomDB {
    id: number;
    correo: string;
    nombre: string;
    apellido: string;
}

export interface IDecodedValidate {
    id: string;
    correo: string;
    iat: number;
    exp: number;
}
export interface IDataError {
    code: string;
    detail: string;
    hint: string;
}

export interface ICiudadanoCustomDB {
    id_ciudadano: number;
    nombre: string;
    primer_apellido: string;
    segundo_apellido: string;
    curp: string;
    genero: string;
}
export interface ILoginCustomDB {
    id_login: number;
    correo: string;
    password: string;
    telcel: string;
    cellphone: string;
    fecha_creacion: Date;
}
export interface ICatalogoCiudadanoCustomDB {
    id_catalogo_ciudadanos: number;
    login_id_login: number;
    ciudadano_id_ciudadano: number;
    fecha_creacion: Date;
    propietario: boolean;
}
