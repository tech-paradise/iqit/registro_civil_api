import * as CiudadanoModel from "./ICiudadanoModels";

export  interface IActaCiudadano extends CiudadanoModel.ICiudadanoDBModel {
    ma_nacimiento: string;
    pa_nacimiento: string;
    nacimiento: string;
    mun_registro: string;
    edo_registro: string;
}
