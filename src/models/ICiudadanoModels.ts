export interface ICiudadanoModelResponse {
    mensaje: string;
    precio: number;
    "registro civil": IRegistroCivil;
    "persona registrada": IPersonaRegistrada;
    "datos filiacion": IDatosFiliacion;
}

export interface IDatosFiliacion {
    nombre_padre: string;
    primer_apellido_padre: string;
    segundo_apellido_padre: string;
    nacionalidad_padre: string;
    nombre_madre: string;
    primer_apellido_madre: string;
    segundo_apellido_madre: string;
    nacionalidad_madre: string;
}

export interface IPersonaRegistrada {
    nombres: string;
    primer_apellido: string;
    segundo_apellido: string;
    sexo: string;
    fecha_nacimiento: string;
    lugar_nacimiento: string;
}

export interface IRegistroCivil {
    curp: string;
    entidad_registro: string;
    municipio_registro: string;
    fecha_registro: string;
    libro: string;
    numero_acta: string;
    numero_entidad: string;
}

export interface ICiudadanoDBModel {
    NUMEROACTA: number;
    ANIOREGISTRO: number;
    TIPODOCUMENTO: number;
    ENTIDADREGISTRO: number;
    MUNICIPIOREGISTRO: number;
    OFICIALIA: number;
    ACTABIS: string;
    CADENA: string;
    CO_FECHA_REGISTRO: Date;
    CO_FECHA_REGISTRO_INC: string;
    CO_LLAVEREGISTROCIVIL: Date;
    CO_FOJA: number;
    CO_TOMO: number;
    CO_LIBRO: number;
    IM_NOMBREORIGINALIMAGEN: string;
    IM_ARCHIVO: Uint8Array;
    OT_NOTASMARGINALES: string;
    OT_CRIP: string;
    OT_VIVOOMUERTO: string;
    PE_PRIMERAPELLIDO: string;
    PE_SEGUNDOAPELLIDO: string;
    PE_NOMBRES: string;
    PE_EDAD: number;
    PE_SEXO: string;
    PE_FECHANACIMIENTO: Date;
    PE_FECHANACIMIENTO_INC: string;
    PE_ENTIDADNACIMIENTO: number;
    PE_MUNICIPIONACIMIENTO: number;
    PE_LOCALIDADNACIMIENTO: string;
    PE_NACIONALIDAD: number;
    PE_PAISNACIMIENTO: number;
    PE_CURP: string;
    PA_PRIMERAPELLIDO: string;
    PA_SEGUNDOAPELLIDO: string;
    PA_NOMBRES: string;
    PA_EDAD: number;
    PA_SEXO: string;
    PA_FECHANACIMIENTO: Date;
    PA_FECHANACIMIENTO_INC: string;
    PA_ENTIDADNACIMIENTO: number;
    PA_MUNICIPIONACIMIENTO: number;
    PA_LOCALIDADNACIMIENTO: string;
    PA_NACIONALIDAD: number;
    PA_PAISNACIMIENTO: number;
    PA_CURP: string;
    PA_NUMEROACTA: number;
    PA_ANIOREGISTRO: number;
    PA_TIPODOCUMENTO: number;
    PA_ENTIDADREGISTRO: number;
    PA_MUNICIPIOREGISTRO: number;
    PA_OFICIALIA: number;
    PA_ACTABIS: string;
    MA_NUMEROACTA: number;
    MA_ANIOREGISTRO: number;
    MA_TIPODOCUMENTO: number;
    MA_ENTIDADREGISTRO: number;
    MA_MUNICIPIOREGISTRO: number;
    MA_OFICIALIA: number;
    MA_ACTABIS: string;
    MA_PRIMERAPELLIDO: string;
    MA_SEGUNDOAPELLIDO: string;
    MA_NOMBRES: string;
    MA_EDAD: number;
    MA_SEXO: string;
    MA_FECHANACIMIENTO: Date;
    MA_FECHANACIMIENTO_INC: string;
    MA_ENTIDADNACIMIENTO: number;
    MA_MUNICIPIONACIMIENTO: number;
    MA_LOCALIDADNACIMIENTO: string;
    MA_NACIONALIDAD: number;
    MA_PAISNACIMIENTO: number;
    MA_CURP: string;
    CN_FECHAACTUALIZACION: Date;
    CN_FECHAACTUALIZACION_INC: string;
    CN_FECHACAPTURA: Date;
    OT_ERRORORIGEN: string;
    OT_FECHAREGISTRONACIMIENTO_INC: string;
    OT_FIRMA: string;
    OT_SELLO: string;
    TMP_FECHA: Date;
    OT_CERTIFICADO_NA: string;
    OT_ULTACTUALIZACION: number;
    TIPOCADENA: number;
    CO_TIPO: string;
    CO_FECHAORIGINAL: Date;
    CO_TRANSCRIPCION: string;
    CO_SOPORTE: Uint8Array;
    P2_EDAD: number;
}
