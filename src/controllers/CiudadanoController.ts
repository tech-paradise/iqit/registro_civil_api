import moment from "moment";
import * as timeStamp from "../helpers/LogUtils" ;
import {ICiudadanoDBModel, ICiudadanoModelResponse, IDatosFiliacion, IPersonaRegistrada, IRegistroCivil} from "../models/ICiudadanoModels";
import * as CiudadanoRoutes from "../routes/CiudadanoRoutes";
import CiudadanoService from "../services/CiudadanoService";

import {Request, Response} from "express";

export default class CiudadanoController {

    public async buscarCiudadano(req: Request, res: Response): Promise<Response> {
        if (req.body.type === CiudadanoRoutes.TypeParameter.CURP) {
            return this.buscarCurp(req, res);
        } else if (req.body.type === CiudadanoRoutes.TypeParameter.DATA) {
            return this.buscarPorInfo(req, res);
        } else {
            res.status(404).json({message: "Opss no debio pasar esto"});
        }
    }

    public async buscarCurp(req: Request, res: Response): Promise<Response> {

        let ciudadano: ICiudadanoModelResponse;
        timeStamp.setHeaderEndPoint("search/get_ciudadano_curp", req.body);
        timeStamp.setMessage("Validando parámetros");

        const error: string = await timeStamp.validateParams(req);
        if (error) {
            return res.status(400).json({message: error });
        }
        timeStamp.setMessage(CiudadanoRoutes.TypeParameter.CURP);
        ciudadano = await this.getCiudadano(req, CiudadanoRoutes.TypeParameter.CURP);

        if (!ciudadano) {
            return res.status(404).json({ message: "Ciudadano no encontrado" });
        }
        return res.json({ciudadano});
    }

    public async buscarPorInfo(req: Request, res: Response): Promise<Response> {
        let ciudadano: ICiudadanoModelResponse;
        timeStamp.setHeaderEndPoint("search/get_ciudadano_info", req.body);
        timeStamp.setMessage("Validando parámetros");

        const error: string = await  timeStamp.validateParams(req);
        if (error) {
            return res.status(400).json({ message: error });
        }
        ciudadano = await this.getCiudadano(req, CiudadanoRoutes.TypeParameter.DATA);

        if (!ciudadano) {
            return res.status(404).json({message: "Ciudadano no encontrado" });
        }
        return res.json(ciudadano);

    }

    protected async getCiudadano(req: Request, search: CiudadanoRoutes.TypeParameter):
        Promise<ICiudadanoModelResponse> {
        // console.log("entre en get ciudadano");
        // tslint:disable-next-line:one-variable-per-declaration
        let ciudadanoResponse: ICiudadanoDBModel
            , bodyRegistroCivil: IRegistroCivil
            , bodyPersonaRegistrada: IPersonaRegistrada,
            bodyDatosFiliacion: IDatosFiliacion;
        const primerApellidoPadre: string =
            req.body.primerApellidoPadre ? req.body.primerApellidoPadre.toUpperCase() : "";
        const segundoApellidoPadre: string =
            req.body.segundoApellidoPadre ? req.body.segundoApellidoPadre.toUpperCase() : "";
        const nombresPadre: string =
            req.body.nombresPadre ? req.body.nombresPadre.toUpperCase() : "";
        const ciudadanoService: CiudadanoService = new CiudadanoService();

        switch (search) {
            case CiudadanoRoutes.TypeParameter.CURP:
                const curp: string = req.body.curp ? req.body.curp.toUpperCase() : "";
                ciudadanoResponse = await  ciudadanoService
                    .findByCurpInfo(curp, nombresPadre, primerApellidoPadre, segundoApellidoPadre);
                break;
            case CiudadanoRoutes.TypeParameter.DATA:
                const nombres: string = req.body.nombres ? req.body.nombres.toUpperCase() : "";
                const primerApellido: string = req.body.primerApellido ? req.body.primerApellido.toUpperCase() : "";
                const segundoApellido: string = req.body.segundoApellido ? req.body.segundoApellido.toUpperCase() : "";
                const sexo: string = req.body.sexo ? req.body.sexo.toUpperCase() : "";
                const numeroEntidad: number = req.body.numeroEntidad ? parseInt(req.body.numeroEntidad, 10) : 0;
                const fechaNacimiento: string = req.body.fechaNacimiento ? req.body.fechaNacimiento : "0000-00-00";
                ciudadanoResponse = await  ciudadanoService.findByInfo(nombres
                    , primerApellido
                    , segundoApellido
                    , sexo,
                    numeroEntidad
                    , fechaNacimiento
                    , nombresPadre
                    , primerApellidoPadre
                    , segundoApellidoPadre);
                break;

        }
        if (!ciudadanoResponse) {
            return null;
        }
        bodyRegistroCivil = {
            curp: ciudadanoResponse.PE_CURP,
            entidad_registro: ciudadanoResponse.ENTIDADREGISTRO.toString(),
            municipio_registro: ciudadanoResponse.MUNICIPIOREGISTRO.toString(),
            fecha_registro: moment(ciudadanoResponse.CO_FECHA_REGISTRO).format("YYYY-MM-DD HH:mm:ss"),
            libro: ciudadanoResponse.CO_LIBRO.toString(),
            numero_acta: ciudadanoResponse.NUMEROACTA.toString(),
            numero_entidad: ciudadanoResponse.ENTIDADREGISTRO.toString()
        };

        bodyPersonaRegistrada = {
            fecha_nacimiento: moment(ciudadanoResponse.PE_FECHANACIMIENTO).format("YYYY-MM-DD HH:mm:ss"),
            lugar_nacimiento: ciudadanoResponse.PE_LOCALIDADNACIMIENTO,
            nombres: ciudadanoResponse.PE_NOMBRES,
            primer_apellido: ciudadanoResponse.PE_PRIMERAPELLIDO,
            segundo_apellido: ciudadanoResponse.PE_SEGUNDOAPELLIDO,
            sexo: ciudadanoResponse.PE_SEXO
        };

        bodyDatosFiliacion = {
            nombre_padre: ciudadanoResponse.PA_NOMBRES,
            primer_apellido_padre: ciudadanoResponse.PA_PRIMERAPELLIDO,
            segundo_apellido_padre: ciudadanoResponse.PA_SEGUNDOAPELLIDO,
            nacionalidad_padre: ciudadanoResponse.PA_NACIONALIDAD.toString(),
            nombre_madre: ciudadanoResponse.MA_NOMBRES,
            primer_apellido_madre: ciudadanoResponse.MA_PRIMERAPELLIDO,
            segundo_apellido_madre: ciudadanoResponse.MA_SEGUNDOAPELLIDO,
            nacionalidad_madre: ciudadanoResponse.MA_NACIONALIDAD.toString()
        };
        return {
            "mensaje": "Ciudadano encontrado",
            "precio": 0,
            "registro civil": bodyRegistroCivil,
            "persona registrada": bodyPersonaRegistrada,
            "datos filiacion": bodyDatosFiliacion
        };
    }
}
