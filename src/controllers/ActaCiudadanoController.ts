import {Request, Response} from "express";
import fs from "fs";
import hbs from "hbs";
import pdf from "html-pdf";
import moment from "moment";
import path from "path";
import qr from "qrcode";
import * as timeStamp from "../helpers/LogUtils";
import {IActaCiudadano} from "../models/IActaCiudadanoModel";
import ActaCiudadanoService from "../services/ActaCiudadanoService";

const pdfStream = (html: string, opt: pdf.CreateOptions): Promise<Buffer> => {
    return new Promise((resolve, reject) =>
        pdf.create(html, opt).toBuffer((err, buffer) =>
            err
                ? reject(err)
                : resolve(buffer)));
};

export default class ActaCiudadanoController {
    public async getActa(req: Request, res: Response): Promise<Response> {
        timeStamp.setHeaderEndPoint("acta/acta", req.body);
        timeStamp.setMessage("Validando parámetros");

        const error: string = await timeStamp.validateParams(req);
        if (error) {
            return res.status(400).json({message: error });
        }
        const cadena: string =
            req.body.cadena ? req.body.cadena.toUpperCase() : "";
        const acta: IActaCiudadano = await new ActaCiudadanoService()
            .findActaInfo(cadena);

        const context: any = acta;
        context.dateRegister = moment(acta.CO_FECHA_REGISTRO).format("DD/MM/YYYY");
        context.dateNacimiento = moment(acta.PE_FECHANACIMIENTO).format("DD/MM/YYYY");
        context.firma = this.getFirmaBase64(acta);
        context.qr =
            await qr.toDataURL(this.getQRTexto(acta), { errorCorrectionLevel: "L" });
        context.img = `data:image/png;base64, ${acta.OT_SELLO}`;
        context.director = "data:image/png;base64"
            + fs.readFileSync(path.join(process.cwd(), "views/firma.jpg")).toString("base64");
        context.margin = "data:image/png;base64"
            + fs.readFileSync(path.join(process.cwd(), "views/plantilla.png")).toString("base64");
        const templateFile: string = fs.readFileSync(path.join(process.cwd(), "views/actaTemplate.hbs"), "utf8");
        const hbsTemplate: HandlebarsTemplateDelegate<any> = hbs.handlebars.compile(templateFile);
        const html: string = hbsTemplate(context);
        // return res.send(html);

        const options: pdf.CreateOptions = {
            format: "Letter",
            border: "0.5cm"
        };
        const bufferPDF = await pdfStream(html, options);
        res.type("application/pdf");
        res.attachment(`acta_${acta.PE_NOMBRES}.pdf`);
        return res.send(bufferPDF);
    }

    private getQRTexto(actaData: IActaCiudadano): string {
        return `Tomo:${actaData.CO_TOMO}|libro:${actaData.CO_LIBRO}|Foja:${actaData.CO_FOJA}|`
            + `Acta:${actaData.NUMEROACTA}|Entidad:${actaData.ENTIDADREGISTRO}|Municipio:${actaData.MUNICIPIOREGISTRO}|`
            + `Registrado:${actaData.PE_NOMBRES} ${actaData.PE_PRIMERAPELLIDO} ${actaData.PE_SEGUNDOAPELLIDO}|`
            + `Padre1:${actaData.PA_NOMBRES} ${actaData.PA_PRIMERAPELLIDO} ${actaData.PA_SEGUNDOAPELLIDO}|`
            + `Padre2:${actaData.MA_NOMBRES} ${actaData.MA_PRIMERAPELLIDO} ${actaData.MA_SEGUNDOAPELLIDO}|`
            + `FechaNacimiento:${moment(actaData.PE_FECHANACIMIENTO).format("DD/MM/YYYY")}|Sexo:${actaData.PE_SEXO}|Cadena:${actaData.CADENA}|`
            + `FechaImpresión:${moment().format("YYYY/MM/DD")}|Impreso en:${actaData.ENTIDADREGISTRO};MEXICO`;
    }

    private  getFirmaBase64(actaData: IActaCiudadano): string {
        return Buffer.from(`${actaData.PE_CURP}|${actaData.PE_NOMBRES}|${actaData.PE_PRIMERAPELLIDO}|`
            + `${actaData.PE_SEGUNDOAPELLIDO}|${actaData.CADENA}|${actaData.PE_SEXO}|`
            + `${actaData.PE_FECHANACIMIENTO}|${actaData.ENTIDADREGISTRO}|null|null`)
            .toString("base64")
            .replace(/(.{2})/g, "$1 ");
    }
}
