import * as bcrypt from "bcryptjs";
import {Request, Response} from "express";
import * as jwt from "jsonwebtoken";
import {TokenExpiredError} from "jsonwebtoken";
import * as timeStamp from "../helpers/LogUtils";
import { IMail, IMailConfirmation, sendMail} from "../helpers/SendMail";
import { IDecodedValidate, IUserCustomDB} from "../models/IUserModel";
import * as Rutas from "../routes/RoutesNames";
import UserService from "../services/UserService";

export default class UserController {
    private readonly userService: UserService;

    public constructor() {
        this.userService = new UserService();
    }
    public async createUser(req: Request, res: Response): Promise<Response> {
        timeStamp.setHeaderEndPoint("userOrEror/register", req.body);
        const error: string = await timeStamp.validateParams(req);
        if (error) {
            return res.status(400).json({ message: error});
        }
        timeStamp.setMessage("Creando usuario");
        // Encriptando contraseña
        const salt: string = bcrypt.genSaltSync(parseInt(process.env.CRYPT_SALT, 10));
        req.body.password = bcrypt.hashSync(req.body.password
            , salt);
        const user: IUserCustomDB = await this.userService.createUser(req);
        let message: string = "";
        if (user) {
            message += "Usuario creado";
            timeStamp.setMessage("Enviando correo");
            const enviado = this.sendEmailValidation(user);
            if (enviado) {
                message += ", por favor, valida tu correo";
            } else {
                message += ", error al enviar correo de validación";
                timeStamp.setError(message);
            }
            timeStamp.setSucess("Usuario creado");
            return res.status(201).json({
                message
            });
        } else {
            timeStamp.setError("al crear usuario");
            message += "Usuario no creado";
            return res.status(500).json({
                message
            });
        }
    }

    // TODO create route for validate email
    public async validateEmail(req: Request, res: Response): Promise<Response | void> {
        timeStamp.setHeaderEndPoint("/confirmation", req.params.toString());
        const {token} = req.params;
        let  decoded: IDecodedValidate;
        let message: string = "No se pudo validar tu información";
        let nombre: string = "";
        let title: string = "Correo no confirmado";
        let valid: boolean = false;
        let userValid: IUserCustomDB;
        try {
            decoded = jwt.verify( token, process.env.JWT_SEDD) as IDecodedValidate;
            userValid = await this.userService.valideUserById(parseInt(decoded.id, 10));
            nombre = `${userValid.nombre} ${userValid.apellido}`;
            message = "Proceso de registro completado";
            valid = true;
            title = "Correo confirmado";
        } catch (e) {
            if (e instanceof TokenExpiredError) {
                message = "El tiempo para validar tu cuenta se ha acabado, registrate de nuevo";
                decoded = jwt.verify( token,
                    process.env.JWT_SEDD,
                    {ignoreExpiration: true}) as IDecodedValidate;
                // TODO delete login in db?
                console.log(message, decoded);
            } else {
                message = "No se pudo validar tu información";
            }
        }
        return res.render("emailConfirmation", { name: nombre, message, title, valid});
    }

    public async login(req: Request, res: Response): Promise<Response> {
        return null;
    }

    private async sendEmailValidation(persona: IUserCustomDB): Promise<boolean> {
        const {id, correo} = persona ;
        const token: string = jwt.sign({ id, correo}, process.env.JWT_SEDD,
        { expiresIn: process.env.CADUCIDAD_TOKEN_EMAIL });
        const url: string = `${Rutas.LOCALHOST + Rutas.API_V1 + Rutas.USER_REGISTER}/${token}` ;
        const mContext: IMailConfirmation = {
            template: "views/emailValidate.hbs",
            context : {
                name : persona.nombre,
                firstName: persona.apellido,
                emailSender: process.env.EMAIL,
                url
            }
        };
        const mailOptions: IMail = {from: process.env.EMAIL,
            subject: "Bienvenido a Registro Civil",
            to: persona.correo
        };
        return await sendMail(mailOptions, mContext);
    }
}
