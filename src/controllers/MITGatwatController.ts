import * as axios from "axios";
import * as crypto from "crypto";
import {Request, Response} from "express";
import * as timeStamp from "../helpers/LogUtils";

export default class MITGatwatController {

    public async finish(req: Request, res: Response): Promise<Response> {
        timeStamp.setHeaderEndPoint("payment/MIT/finish", req.body);
        return res.json({message: "Works!!!"});
    }

    public async payment(req: Request, res: Response): Promise<Response> {
        timeStamp.setHeaderEndPoint("payment/MIT", req.body);
        timeStamp.setMessage("Validando parámetros");
        const error: string = await  timeStamp.validateParams(req);
        if (error) {
            return res.status(400).json({ message: error });
        }

        // Se crea la estructura del xml para enviarsela a la pasarela de pagos
        const xmlString: string =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<P>" +
            "<business>" +
            "<id_company>SNBX</id_company>" +
            "<id_branch>01SNBXBRNCH</id_branch>" +
            "<user>SNBXUSR01</user>" +
            "<pwd>SECRETO</pwd>" +
            "</business>" +
            "<url>" +
            "<reference>Acta de Nacimiento 17112019</reference>" + // reference debe ser un folio unico, diseñar el algoritmo
            "<amount>" + req.body.amount + "</amount>" +
            "<moneda>MXN</moneda>" +
            "<canal>W</canal>" +
            "<omitir_notif_default>1</omitir_notif_default>" +
            "</url>" +
            "</P>";

        // Llave para en HEX para prueba SANDBOX
        const keyPayment: string = "5DCC67393750523CD165F17E1EFADD21";
        // iv_payment se definira de manera dinamica
        const ivPayment: string = "0000000000000000";
        const endpoint: string = "https://wppsandbox.mit.com.mx/gen";

        // La llave se pasara a sistema binario para la obtencion de 128 bits
        const key: string = Array.prototype.slice.call( Buffer.from(keyPayment, "hex"), 0);

        // Codificacion en utf8
        const key2: Buffer = Buffer.from(key, "utf8");
        const iv: Buffer = Buffer.from(ivPayment, "utf8");

        // Encriptacion por aes-128-cbc
        const cipher: crypto.Cipher = crypto.createCipheriv("aes-128-cbc", key2, iv);
        let encrypted: string = cipher.update(xmlString, "utf8", "base64");
        encrypted += cipher.final("base64");

        const sendData: string =
            "xml=" +
            "<pgs>" +
            "<data0>SNDBX123</data0>" +
            "<data>" + encrypted + "</data>" +
            "</pgs>";

        const encodeString: string = encodeURIComponent(sendData);

        // POST para envio de la info a la pasarela de pagos
        try {
            const responseAxios: axios.AxiosResponse = await axios.default
                .post(endpoint, `xml=${encodeString}`,
                    {headers: {"content-type": "application/x-www-form-urlencoded", "cache-control": "no-cache"}});
            const decipher: crypto.Decipher = crypto.createDecipheriv("aes-128-cbc", key2, iv);
            let decrypted: Buffer = decipher.update(responseAxios.data, "base64");
            decrypted = Buffer.concat([decrypted, decipher.final()]);

            // retorna informacion encriptada, se desencripta y obtengo unicamente la url,
            // diseñar respuestas para casos de error
            const urlPayment = decrypted.toString().substring(decrypted.lastIndexOf("https"), decrypted.lastIndexOf("</nb_url>"));
            res.json({url: urlPayment});
        } catch (e) {
            timeStamp.setError(e);
        }
    }
}
