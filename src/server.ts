import {Config, ConnectionConfig} from "knex";
import {IGeneratorSchema, ISchemaDb} from "./config/ISchemaDb";
import {Properties} from "./config/properties";
import * as timeStamp from "./helpers/LogUtils";
import Server from "./server/Server";
// Archivo Main

// iniciando propeidadses
Properties.init();

// Configurando servidor
const server: Server = Server.init(parseInt(process.env.PORT, 10));

// Iniciando servidor
server.start(() => {
    timeStamp.setMessage(`Server on Port ${process.env.PORT}`);
    const infoDB: Config = JSON.parse(process.env.SCHEMA_GENERATOR_SQL);
    const infoDBCustom: Config = JSON.parse(process.env.SCHEMA_GENERATOR_SQL_CUSTOM);
    timeStamp.setMessage(`Usuario DB: ${(infoDB.connection as ConnectionConfig).user}\nHost: ${(infoDB.connection as ConnectionConfig).host}\nDatabase: ${(infoDB.connection as ConnectionConfig).database}\nUrl: ${process.env.URL}`);
    timeStamp.setMessage(`Usuario DB: ${(infoDB.connection as ConnectionConfig).user}\nHost: ${(infoDB.connection as ConnectionConfig).host}\nDatabase: ${(infoDB.connection as ConnectionConfig).database}\n`);
});
