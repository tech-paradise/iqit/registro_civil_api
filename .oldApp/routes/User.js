const express = require('express');
const router = express.Router();
const UserController=require('../controllers/User');

router.post('/register',UserController.validate('createUser'),UserController.createUser);


module.exports=router;
