const express = require('express');
const router = express.Router();
const routerBuscarCiudadano=require('./Ciudadano').router;
const routerUser=require('./User');


router.use('/search',routerBuscarCiudadano);
router.use ('/user',routerUser);

module.exports=router;
