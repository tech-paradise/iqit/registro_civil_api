const express = require('express');
const router = express.Router();
const CiudadanoController=require('../controllers/Ciudadano');

router.get('/', (req,res)=>{
    res.json({
        status:'sucessfull',
        message:'api work'
    })
});

router.post('/get_ciudadano_curp',
    CiudadanoController.validate(CiudadanoController.VALIDATE_CURP),
    CiudadanoController.buscarPorCurp);

router.post('/get_ciudadano_datos',
    CiudadanoController.validate(CiudadanoController.VALIDATE_DATOS),
    CiudadanoController.buscarPorInfo);


router.post('/get_acta',CiudadanoController.validate(CiudadanoController.VALIDATE_ACK),CiudadanoController.getActa);

module.exports={
    router
};