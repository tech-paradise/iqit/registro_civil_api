// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;
// ============================
//  URL serviod
// ============================
process.env.URL = process.env.URL||'http://35.237.6.187:3000';
process.env.URL_DB= process.env.URL_DB || '35.237.6.187';
// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'devPg';

// ============================
//  Vencimiento del Token
// ============================
// 60 segundos
// 60 minutos
// 24 horas
// 30 días
//process.env.CADUCIDAD_TOKEN = '168h';
//process.env.CADUCIDAD_TOKEN_EMAIL = '3d';

// ============================
//  SEED de autenticación
// ============================
process.env.CRYPT_SALT = process.env.CRYPT_SALT || 10;
process.env.JWT_SEDD = process.env.JWT_SEDD || 'sed-de-api-registro-civil';

// ============================
//  Base de datos
//  Direccion de servidor
// ============================
let schemaDB;

if (process.env.NODE_ENV === 'devPg') {
    schemaDB = {
        user: 'registro',
        host: process.env.URL_DB,
        database: 'registro_civil',
        password: 'test',
        port: 5432
    };
} else {
    schemaDB = {
        user: 'registro',
        host: process.env.URL_DB,
        database: 'registro_civil',
        password: 'test',
        port: 5432
    };
}
process.env.SCHEMA_DB = JSON.stringify(schemaDB);
