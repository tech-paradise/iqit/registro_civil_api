const { Pool, Client } = require("pg");
const pool = new Pool(JSON.parse(process.env.SCHEMA_DB));

let emailExist= async email=>{
    let res;
    let sql = "select * from interconexion.login \n" +
        "where correo=$1";
    try {
        res = await pool.query(sql, [email]); //lat and lon go in reverse order
    } catch (error) {
        console.log("error", error);
        return true;
    }
    //console.log(res);
    return res.rowCount > 0;
};

let createUser=async req=>{
    let {name,fName,sName,curp,cellphone,email,genero,passwor}=req.body;
    let sql= `select * from interconexion.agregar_usuario(
        $1,$2,$3,$4,$5,$6,$7,$8
    );`;
    try {
        res = await pool.query(sql, [name,fName,sName,curp,cellphone,email,genero,passwor]); //lat and lon go in reverse order
    } catch (error) {
        console.log("error", error);
        return false;
    }
    console.log(res);
    return  true;
};
module.exports={
    emailExist,
    createUser
};
