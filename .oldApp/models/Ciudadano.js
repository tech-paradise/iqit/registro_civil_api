const { Pool, Client } = require("pg");
const pool = new Pool(JSON.parse(process.env.SCHEMA_DB));


let findByCurp = async (curp='',nombresPadre='',primerApellidoPadre='',segundoApellidoPadres='') => {
    let res;
    let sql = "select * from interconexion.nrc_nacimientos \n" +
        "where pe_curp=$1 \n" +
        "and ((\n" +
        "\tpa_nombres=$2 \n" +
        "\tand pa_primerapellido=$3 \n" +
        "\tand pa_segundoapellido=$4) \n" +
        "\tor(ma_nombres=$2\n" +
        "\tand ma_primerapellido=$3\n" +
        "\tand ma_segundoapellido=$4))";
    try {
        res = await pool.query(sql, [curp,nombresPadre,primerApellidoPadre,segundoApellidoPadres]); //lat and lon go in reverse order
    } catch (error) {
        console.log("error", error);
        return false;
    }
    //console.log(res);
    return res.rowCount > 0 ? res.rows[0] : false;
};


let findByInfo=async (nombre,primerApellido,segundoApellifo,sexo,numeroEntidad,fechaNac,nombresPadre,primerApellidoPadre,segundoApellidoPadres)=>{
    let res;
    let sql='select * from interconexion.nrc_nacimientos \n' +
        'where pe_nombres =$1 and pe_primerapellido=$2\n' +
        'and pe_segundoapellido=$3 and pe_sexo=$4\n' +
        'and entidadregistro=$5 and pe_fechanacimiento=$6\n' +
        'and ((\n' +
        '\tpa_nombres=$7 \n' +
        '\tand pa_primerapellido=$8 \n' +
        '\tand pa_segundoapellido=$9) \n' +
        '\tor(ma_nombres=$7\n' +
        '\tand ma_primerapellido=$8\n' +
        '\tand ma_segundoapellido=$9))';
    try {
        res= await pool.query(sql,[nombre,primerApellido,segundoApellifo,sexo,numeroEntidad,fechaNac,nombresPadre,primerApellidoPadre,segundoApellidoPadres]);
    }catch (e) {
        console.log('error: ',e);
        return false;
    }
    return res.rowCount > 0 ? res.rows[0] : false;
};

let existCurp=async (curp)=>{
    let res;
    let sql = "select * from interconexion.nrc_nacimientos \n" +
        "where pe_curp=$1";
    try {
        res = await pool.query(sql, [curp]); //lat and lon go in reverse order
    } catch (error) {
        console.log("error", error);
        return false;
    }
    //console.log(res);
    return res.rowCount > 0;
};

module.exports={
    findByCurp,
    findByInfo,
    existCurp
};
