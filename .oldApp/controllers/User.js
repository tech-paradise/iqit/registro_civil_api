const { check, body } = require("express-validator/check");
const passwordValidator = require("password-validator");
const {validateParams}=require('../helpers/validateParams');
const timeStamp=require("../helpers/timeStamp");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const UserModel=require('../models/User');
const  CiudadanoModel=require('../models/Ciudadano');


let schema = new passwordValidator();
schema
    .is().min(8)
    .is().max(16)
    .has().digits()
    .has().letters();

let validate = (method) => {
    switch (method) {
        case "createUser": {
            // console.log("entre1");
            return [body("name", "nombre invalido").isString().trim(),
                body("fName", "apellido invalido")
                    .isString()
                    .trim(),
                body("sName")
                    .optional()
                    .isString()
                    .trim(),
                body("curp", "es campo obligatorio")
                    .optional()
                    .isString().
                    custom(async (curp)=> {
                        if (!await CiudadanoModel.existCurp(curp)) throw new Error("No existe Curp");
                        return true;
                    }),
                body("cellphone", "es campo obligatorio")
                    .isMobilePhone(),
                body("email", "email invalido")
                    .isEmail()
                    .custom(async email => {
                        if (await UserModel.emailExist(email)) throw new Error("email registrado");
                        return true;
                    }),
                body("genero", "es campo obligatorio")
                    .isIn(['f','m','F','M']),
                body("password")
                    .custom(password =>{
                        if (!schema.validate(password) ) throw new Error("password debe tener entre 8 y 16 caracteres, algún numero y letra");
                        return true;
                    }),
                body("terminos", "debe ser booleano")
                    .isBoolean().toBoolean()
                    .custom(terminos=> {
                        if (terminos === false) throw new Error("se debe aceptar los terminos");
                        return true;
                    })
            ];
        }
        case "loginUser": {
            //console.log("entre2");
            return [body("email", "formato invalido").isEmail(),
                body("idDevice", "es obligatorio").exists(),
                body("password", "obligatorio").exists(),
                body("instrumentoId", "obligatorio").isString()];
        }
    }
};

let createUser = async (req, res) => {
    timeStamp.setHeaderEndPoint("user/register",req.body);
    let error=await validateParams(req);
    if (error) {
        return res.status(400).json({ status: 'failed', message: error });
    }
    //console.log("controller",req);
    timeStamp.setMessage("Creando usuario");
    timeStamp.setMessage(` process:  ${process.env.CRYPT_SALT}`);

    let salt=bcrypt.genSaltSync(parseInt(process.env.CRYPT_SALT));
    timeStamp.setMessage(` process2:  ${salt}`);
    req.body.password= bcrypt.hashSync(req.body.password
        , salt);
    timeStamp.setMessage(` process2:  ${salt}`);
    if (await UserModel.createUser(req)) {
        /*timeStamp.setMessage("Enviando correo");
        let enviado=sendEmailValidation(persona);
        if (enviado){
            message+=", por favor, valida tu correo";
        }else{
            message+=", error al enviar correo de validación";
            timeStamp.setError(message);
        }*/
        timeStamp.setSucess("Usuario creado");
        return res.json({
            status: "sucessfull",
            message:'si'
        });
    }else{
        timeStamp.setError("al crear usuario");
        return res.json({
            status: "failed",
            message: "Usuario no creado"
        });
    }
};

module.exports={
    createUser,
    validate
};

