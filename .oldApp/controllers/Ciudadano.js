const {check}=require('express-validator');
const timeStamp=require('../helpers/timeStamp');
const  ciudadanoModel=require('../models/Ciudadano');
const validateParams=require('../helpers/validateParams');
const moment=require('moment');
const path=require('path');
const  VALIDATE_CURP='curp';
const  VALIDATE_DATOS='datos';
const  VALIDATE_ACK='recibo';

let validate=(method)=>{
    console.log('aqui2');
    switch (method) {
        case VALIDATE_CURP: {
            // console.log("entre1");
            return [check("curp", "--> curp campo obligatorio").isString().trim(),
                check("nombresPadre", "--> nombre campo obligatorio").isString().trim(),
                check("primerApellidoPadre",'--> primer apellido campo obligatorio').isString().trim(),
                check("segundoApellidoPadre",'--> segundo apellido campo obligatorio').isString().trim()];
        }
        case VALIDATE_DATOS: {
            // console.log("entre1");
            return [check("nombres", "--> los nombres son un campo obligatorio").isString().trim(),
                check("primerApellido", "--> el primer apellido es un campo obligatorio").isString().trim(),
                check("segundoApellido", '--> el segundo apellido campo obligatorio').isString().trim(),
                check('sexo','--> el campo sexo debe ser una letra permitida').trim().isIn(['m','M','F','f']),
                check ('numeroEntidad', '--> el campo numero de entidad es obligatorio y entero').trim().isInt(),
                check("fechaNacimiento", '--> la fecha de nacimiento con formato incorrecto').trim().isISO8601(),
                check("nombresPadre", "--> nombre campo obligatorio").isString().trim(),
                check("primerApellidoPadre",'--> primer apellido campo obligatorio').isString().trim(),
                check("segundoApellidoPadre",'--> segundo apellido campo obligatorio').isString().trim()];
        }
            case VALIDATE_ACK: {
                return [check('folioRecibo','--> el folio del recibo de pago es campo obligatorio').isString().trim()]
            }
    }
};


let buscarPorCurp=async (req, res)=>{
    timeStamp.setHeaderEndPoint("search/get_ciudadano_curp",req.body);
  //console.log(req.body);
    timeStamp.setMessage("Validando parámetros");
    let error=await validateParams.validateParams(req);
    if (error) {
        return res.status(400).json({ status: 'failed', message: error });
    }
    ciudadano=await getCiudadano(req,VALIDATE_CURP);

    if (!ciudadano){
        return res.status(400).json({ status: 'invalido', message: 'Ciudadano no encontrado' });
    }
    res.json(ciudadano);
};

let buscarPorInfo=async (req, res)=>{
    timeStamp.setHeaderEndPoint("search/get_ciudadano_info",req.body);
    //console.log(req.body);
    timeStamp.setMessage("Validando parámetros");
    let error=await  validateParams.validateParams(req);
    if (error) {
        return res.status(400).json({ status: 'failed', message: error });
    }
    ciudadano=await getCiudadano(req,VALIDATE_DATOS);

    if (!ciudadano){
        return res.status(400).json({ status: 'invalido', message: 'Ciudadano no encontrado' });
    }
    res.json(ciudadano);
};

let getActa=async (req,res)=>{
    timeStamp.setHeaderEndPoint("search/get_ciudadano_acta",req.body);
    timeStamp.setMessage("Validando parámetros");
    let error=await  validateParams.validateParams(req);
    if (error) {
        return res.status(400).json({ status: 'failed', message: error });
    }
    return res.download(path.join(path.resolve(__dirname,'../helpers'),'ActaTest.pdf'),'Acta Nacimiento.pdf');
};

let getCiudadano=async (req,search)=>{
    let ciudadano,bodyRegistroCivil,bodyPersonaRegistrada,bodyDatosFiliacion;
    let primerApellidoPadre=req.body.primerApellidoPadre?req.body.primerApellidoPadre.toUpperCase():'';
    let segundoApellidoPadre=req.body.segundoApellidoPadre?req.body.segundoApellidoPadre.toUpperCase():'';
    let nombresPadre=req.body.nombresPadre?req.body.nombresPadre.toUpperCase():'';
    switch(search){
        case VALIDATE_CURP:
            let curp=req.body.curp?req.body.curp.toUpperCase():'';
            ciudadano=await  ciudadanoModel.findByCurp(curp,nombresPadre,primerApellidoPadre,segundoApellidoPadre);
            break;
        case VALIDATE_DATOS:
            let nombres=req.body.nombres?req.body.nombres.toUpperCase():'';
            let primerApellido=req.body.primerApellido?req.body.primerApellido.toUpperCase():'';
            let segundoApellido=req.body.segundoApellido?req.body.segundoApellido.toUpperCase():'';
            let sexo=req.body.sexo?req.body.sexo.toUpperCase():'';
            let numeroEntidad=req.body.numeroEntidad?parseInt(req.body.numeroEntidad):0;
            let fechaNacimiento=req.body.fechaNacimiento?req.body.fechaNacimiento:'0000-00-00';
            ciudadano=await  ciudadanoModel.findByInfo(nombres
                ,primerApellido
                ,segundoApellido
                ,sexo,
                numeroEntidad
                ,fechaNacimiento
                ,nombresPadre
                ,primerApellidoPadre
                ,segundoApellidoPadre);
            break;

    }
    if(!ciudadano){
        return false;
    }
    bodyRegistroCivil={
        curp:ciudadano.pe_curp,
        entidad_registro:ciudadano.entidadregistro,
        municipio_registro:ciudadano.municipioregistro,
        fecha_registro:moment(ciudadano.co_fecha_registro).format('YYYY-MM-DD HH:mm:ss'),
        libro:ciudadano.co_libro,
        numero_acta:ciudadano.numeroacta,
        numero_entidad:ciudadano.entidadregistro
    };

    bodyPersonaRegistrada={
        nombres:ciudadano.pe_nombres,
        primer_apellido:ciudadano.pe_primerapellido,
        segundo_apellido:ciudadano.pe_segundoapellido,
        sexo:ciudadano.pe_sexo,
        fecha_nacimiento:moment(ciudadano.pe_fechanacimiento).format('YYYY-MM-DD HH:mm:ss'),
        lugar_nacimiento:ciudadano.pe_localidadnacimiento

    };

    bodyDatosFiliacion={
        nombre_padre:ciudadano.pa_nombres,
        primer_apellido_padre:ciudadano.pa_primerapellido,
        segundo_apellido_padre:ciudadano.pa_segundoapellido,
        nacionalidad_padre:ciudadano.pa_nacionalidad,
        nombre_madre:ciudadano.ma_nombres,
        primer_apellido_madre:ciudadano.ma_primerapellido,
        segundo_apellido_madre:ciudadano.ma_segundoapellido,
        nacionalidad_madre:ciudadano.ma_nacionalidad
    };
    return {
        status: 'valido',
        mensaje: 'Ciudadano encontrado',
        precio: 0,
        'registro civil': bodyRegistroCivil,
        'persona registrada':bodyPersonaRegistrada,
        'datos filiacion':bodyDatosFiliacion
    };
};

module.exports={
    validate,
    buscarPorCurp,
    buscarPorInfo,
    getActa,
    ////CONSTANTES
    VALIDATE_CURP,
    VALIDATE_DATOS,
    VALIDATE_ACK
};