const timeStamp=require('./timeStamp');
const {validationResult}=require('express-validator');

let validateParams=async (req)=>{
    let result = await validationResult (req);
    timeStamp.setMessage("Validando parámetros");
    if (!result.isEmpty()) {
        let error = result.array().map(i => `error en ${i.param} ${i.msg}`).join(', ');
        timeStamp.setErrorParam(result);
        return error;
    }else{
        return false;
    }
};

module.exports={
    validateParams
};