const moment = require('moment');

let getNow=()=>{
    return `[${moment().format('YYYY/MM/DD-HH:mm:ss')}] api_registro_civil : `;
};
let setHeaderEndPoint=(endpoint, body)=>{
    console.log(`${getNow()}+++++++++++++++++++++++ En endpoint ${endpoint}`);
    console.log(getNow(),"Body recibido:\n",body);
};

let setErrorParam=(result)=>{
    let params=result.array().map(i=>`${JSON.stringify(i.param)}`).join('\n');
    console.log(`${getNow()}-------- Parametros erroneos: \n${params}`);
};

let getHeaderParam=()=>{
    console.log(`${getNow()}`);
};

let setHeaderCreate=(create)=>{
    console.log(`${getNow()}Creando ${create}`);
};
let setError=(error)=>{console.log(`${getNow()}-------- Error ${error}`)};

let setMessage=(messg)=>{console.log(`${getNow()}${messg}`)};

let setSucess=(success)=>{console.log(`${getNow()}++++++++ Hecho ${success}`)};

module.exports={
    getNow,
    setHeaderEndPoint,
    setErrorParam,
    getHeaderParam,
    setHeaderCreate,
    setError,
    setMessage,
    setSucess
};
